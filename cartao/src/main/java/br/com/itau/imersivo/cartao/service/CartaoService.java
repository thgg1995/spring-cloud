package br.com.itau.imersivo.cartao.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.imersivo.cartao.model.Cartao;
import br.com.itau.imersivo.cartao.repository.CartaoRepository;

@Service
public class CartaoService {

	@Autowired
	CartaoRepository cartaoRepository;
	
	public Cartao criarCartao(Cartao cartao) {
		return cartaoRepository.save(cartao);
	}
	
	public Iterable<Cartao> listarCartoes() {
		return cartaoRepository.findAll();
	}
	
}
