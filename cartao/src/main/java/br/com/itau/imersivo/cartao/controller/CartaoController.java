package br.com.itau.imersivo.cartao.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersivo.cartao.model.Cartao;
import br.com.itau.imersivo.cartao.service.CartaoService;

@RestController
public class CartaoController {

	@Autowired
	CartaoService cartaoService;
	
	@PostMapping("/cartao")
	public void criarCartao(@RequestBody Cartao cartao) {
		cartaoService.criarCartao(cartao);
	}
	
	@GetMapping("/cartao")
	public Iterable<Cartao> listarCartoes() {
		return cartaoService.listarCartoes();
	}
	
}
