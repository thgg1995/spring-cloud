package br.com.itau.imersivo.cartao.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.itau.imersivo.cartao.model.Cartao;

@Repository
public interface CartaoRepository extends CrudRepository<Cartao, Long>{

}
